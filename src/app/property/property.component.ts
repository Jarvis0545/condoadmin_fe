import { Component, OnInit } from '@angular/core';
import { Property } from '../classes/property-model';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-property',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.css']
})
export class PropertyComponent implements OnInit {
  
  apiUrl = 'https://localhost:5001/Property';
  today = new Date();

  httpOptions = { 
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Acces-Control-Allow-Origin': '*'
    })
  }
  properties: Property[];
  property: Property;

  constructor(private http: HttpClient) { 
    this.getProperties();
  }

  ngOnInit(): void {
  }
  getProperties(){
    this.http.get<Property[]>(this.apiUrl).subscribe(r => this.properties = r);
  }

  addProperty(property: Property){
    try{
      this.http.post<Property>(this.apiUrl, property, this.httpOptions)
      .subscribe();
      alert("Nueva propiedad agregada")
      this.getProperties();
    }catch(e){
      console.error(e);
    }
  }

  

}
