import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Payment } from '../classes/payment-model';
import { Customer} from '../classes/customer-model';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  apiUrl = 'https://localhost:5001/Payment';
  today = new Date();

  httpOptions = { 
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Acces-Control-Allow-Origin': '*'
    })
  }
  customersPayments: Payment[];
  customer: Customer;
  payments: Payment[];
  payment: Payment;
  

  constructor( private http: HttpClient) { 
    this.getPayments();
  }

  ngOnInit(): void {
  }

  getPayments(){
    this.http.get<Payment[]>(this.apiUrl).subscribe(r => this.payments = r);
  }

  getPaymentsById(data){
    alert(this.apiUrl+'/'+ data.id );
    this.http.get<Payment[]>(this.apiUrl+'/'+data.id).subscribe(r => this.payments = r);    
  }

  addPayment(payment: Payment){
    try{
      payment.generated = this.today;
      this.http.post<Payment>(this.apiUrl, payment, this.httpOptions)
      .subscribe();
      alert('nuevo pago ha sido agregado');
      this.getPayments();
    }catch(e){
      console.error(e);
    }
  }

  payPayment(data){
    try{
      //var pago: Payment;
      alert(this.apiUrl+'/GetPayment/'+ data.id);
      this.http.get<Payment>(this.apiUrl+'/GetPayment/'+ data.id).subscribe(r => this.payment = r);
      // console.log(this.payment);
      this.payment.payment = true;
      //console.log(this.payment);
      this.http.put<Payment>(this.apiUrl, this.payment, this.httpOptions).subscribe();
      
      alert('El pago numero: '+ this.payment.id + ' se ha realizado');
      this.getPayments();
    }catch(e){
      console.error(e);
    }

  }

}
