import { Component, OnInit } from '@angular/core';
import { Respository } from '../classes/repository';
import { Customer } from '../classes/customer-model';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})

export class CustomerComponent implements OnInit {
  apiUrl = 'https://localhost:5001/Customer';
  constructor(private http: HttpClient) { 
    this.getCustomers()
  }

  ngOnInit(): void {
  }

  httpOptions = { 
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Acces-Control-Allow-Origin': '*'
    })
  }
  
  customers: Customer[];

  getCustomers(){
    this.http.get<Customer[]>(this.apiUrl).subscribe(r => this.customers = r);
  }

  addCustomer(customer: Customer){
    try{
      this.http.post<Customer>(this.apiUrl, customer, this.httpOptions)
      .subscribe()
      alert(customer.name + ' ha sido agregada');
    }catch(e){
      console.error(e);
    }
  }

}
