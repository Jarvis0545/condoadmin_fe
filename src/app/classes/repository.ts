import { Customer } from './customer-model';
import { Property } from './property-model';

import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse} from '@angular/common/http';
// import { Observable } from 'rxjs/Observable';
// import 'rxjs/add/operator/map';
//import { } from './'

const apiUrl = 'https://localhost:5001/';

@Injectable()
export class Respository{
    constructor(private http: HttpClient) { 
        this.getCustomers();
    }
    customers: Customer[];
    customer: Customer;

    getCustomers(){
        let url = apiUrl + '/Customer/';
        this.http.get<Customer[]>(url).subscribe(response => this.customers = response);
    }


}