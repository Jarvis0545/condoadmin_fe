import { Customer } from './customer-model';

export class Payment{
    constructor(
        public id?: number,
        public amount?: number,
        public payment?: boolean,
        public generated?: Date,
        public payDay?: Date,
        public customerId?: number,
        public customer?: Customer
    ) {}
}