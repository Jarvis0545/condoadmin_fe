import { Customer } from './customer-model';

export class Property{
    constructor(
        public id?: number,
        public number?: number,
        public apto?: number,
        public customerId?: number,
        public customer?: Customer
    ) { }
}