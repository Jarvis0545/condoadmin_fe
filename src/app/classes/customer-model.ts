import { Property } from './property-model';

export class Customer{
    constructor(
        public id?: number,
        public name?: string,
        public phone?: string,
        public email?: string,
        public active?: boolean,
        public properties?: Property[]
    ) { }
}